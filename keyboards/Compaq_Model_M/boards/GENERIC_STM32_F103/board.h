/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef _BOARD_H_
#define _BOARD_H_

/*
 * Setup for a Generic STM32F103 board.
 */

/*
 * Board identifier.
 */
#define BOARD_GENERIC_STM32_F103
#define BOARD_NAME              "Generic STM32F103x board"

/*
 * Board frequencies.
 */
#define STM32_LSECLK            32768
#define STM32_HSECLK            8000000

/*
 * MCU type, supported types are defined in ./os/hal/platforms/hal_lld.h.
 */
#define STM32F103xB
//#define STM32F103xE

/*
 * IO pins assignments
 */

/* on-board */

/*
#define LED_CAPS          PAL_LINE(GPIOB, 12)
#define LED_NUM           PAL_LINE(GPIOA, 1)
#define LED_SCRL          PAL_LINE(GPIOC, 5)
#define XT_CLK            PAL_LINE(GPIOB, 14)
#define XT_DAT            PAL_LINE(GPIOB, 15)
#define LED_USB           PAL_LINE(GPIOC, 4)


// In case your board has a "USB enable" hardware
// controlled by a pin, define it here. (It could be just
// a 1.5k resistor connected to D+ line.)

// #define GPIOC_BACKLIGHT_PIN               15

#define GPIOB_USB_DISC          8

#define LINE_ROW_1                  PAL_LINE(GPIOB, 0)   // ESC ok
#define LINE_ROW_2                  PAL_LINE(GPIOB, 1)   // TAB ok
#define LINE_ROW_3                  PAL_LINE(GPIOD, 4)   // F1 ok
#define LINE_ROW_4                  PAL_LINE(GPIOD, 5)   // 2 ok
#define LINE_ROW_5                  PAL_LINE(GPIOD, 11)  // W ok
#define LINE_ROW_6                  PAL_LINE(GPIOB, 5)   // A ok
#define LINE_ROW_7                  PAL_LINE(GPIOB, 6)   // X ok
#define LINE_ROW_8                  PAL_LINE(GPIOB, 7)   // B ok

#define LINE_COL_1                  PAL_LINE(GPIOD, 14)  // A ok
#define LINE_COL_2                  PAL_LINE(GPIOD, 15)  // S ok
#define LINE_COL_3                  PAL_LINE(GPIOD, 0)   // D ok
#define LINE_COL_4                  PAL_LINE(GPIOD, 1)   // F ok
#define LINE_COL_5                  PAL_LINE(GPIOE, 7)   // J ok
#define LINE_COL_6                  PAL_LINE(GPIOE, 8)   // L ok
#define LINE_COL_7                  PAL_LINE(GPIOE, 9)   // # ok
#define LINE_COL_8                  PAL_LINE(GPIOE, 10)  // ENT ok 
#define LINE_COL_9                  PAL_LINE(GPIOE, 11)  // COMM ok 
#define LINE_COL_10                 PAL_LINE(GPIOE, 12)  // DOWN ok
#define LINE_COL_11                 PAL_LINE(GPIOE, 13)  // RIGHT ok 
#define LINE_COL_12                 PAL_LINE(GPIOE, 14)  // LSHFT ok
#define LINE_COL_13                 PAL_LINE(GPIOE, 15)  // LALT ok
#define LINE_COL_14                 PAL_LINE(GPIOD, 8)   // LEFT ok
#define LINE_COL_15                 PAL_LINE(GPIOD, 9)   // P3 ok
#define LINE_COL_16                 PAL_LINE(GPIOD, 10)  // RCTRL ok
*/
#define LED_CAPS          PAL_LINE(GPIOB, 15)
#define LED_NUM           PAL_LINE(GPIOB, 15)
#define LED_SCRL          PAL_LINE(GPIOB, 15)
#define XT_CLK            PAL_LINE(GPIOB, 15)
#define XT_DAT            PAL_LINE(GPIOB, 15)
#define LED_USB           PAL_LINE(GPIOB, 2)

#define GPIOA_USB_DISC          3

#define LINE_ROW_1                  PAL_LINE(GPIOA, 15)   // 
#define LINE_ROW_2                  PAL_LINE(GPIOB, 3)   // 
#define LINE_ROW_3                  PAL_LINE(GPIOB, 4)   // 
#define LINE_ROW_4                  PAL_LINE(GPIOB, 5)   // 
#define LINE_ROW_5                  PAL_LINE(GPIOA, 5)  // 
#define LINE_ROW_6                  PAL_LINE(GPIOB, 10)   // 
#define LINE_ROW_7                  PAL_LINE(GPIOB, 11)   // 
#define LINE_ROW_8                  PAL_LINE(GPIOB, 12)   // 

#define LINE_COL_1                  PAL_LINE(GPIOB, 1)  // 
#define LINE_COL_2                  PAL_LINE(GPIOB, 0)  // 
#define LINE_COL_3                  PAL_LINE(GPIOA, 7)   // 
#define LINE_COL_4                  PAL_LINE(GPIOA, 6)   // 
#define LINE_COL_5                  PAL_LINE(GPIOA, 4)   // 
#define LINE_COL_6                  PAL_LINE(GPIOB, 14)   // 
#define LINE_COL_7                  PAL_LINE(GPIOB, 13)   // 
#define LINE_COL_8                  PAL_LINE(GPIOA, 1)  // 
#define LINE_COL_9                  PAL_LINE(GPIOA, 0)  // 
#define LINE_COL_10                 PAL_LINE(GPIOC, 15)  // 
#define LINE_COL_11                 PAL_LINE(GPIOC, 14)  // 
#define LINE_COL_12                 PAL_LINE(GPIOC, 13)  // 
#define LINE_COL_13                 PAL_LINE(GPIOB, 6)  // 
#define LINE_COL_14                 PAL_LINE(GPIOB, 7)   // 
#define LINE_COL_15                 PAL_LINE(GPIOB, 8)   // 
#define LINE_COL_16                 PAL_LINE(GPIOB, 9)  // 

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 *
 * The digits have the following meaning:
 *   0 - Analog input.
 *   1 - Push Pull output 10MHz.
 *   2 - Push Pull output 2MHz.
 *   3 - Push Pull output 50MHz.
 *   4 - Digital input.
 *   5 - Open Drain output 10MHz.
 *   6 - Open Drain output 2MHz.
 *   7 - Open Drain output 50MHz.
 *   8 - Digital input with PullUp or PullDown resistor depending on ODR.
 *   9 - Alternate Push Pull output 10MHz.
 *   A - Alternate Push Pull output 2MHz.
 *   B - Alternate Push Pull output 50MHz.
 *   C - Reserved.
 *   D - Alternate Open Drain output 10MHz.
 *   E - Alternate Open Drain output 2MHz.
 *   F - Alternate Open Drain output 50MHz.
 * Please refer to the STM32 Reference Manual for details.
 */

/*
 * Port A setup.
 * Everything input with pull-up except:
 * PA2  - Alternate output  (USART2 TX).
 * PA3  - Normal input      (USART2 RX).
 * PA9  - Alternate output  (USART1 TX).
 * PA10 - Normal input      (USART1 RX).
 */
//#define VAL_GPIOACRL            0x88884B88      /*  PA7...PA0 */
#define VAL_GPIOACRL            0x88884b88      /*  PA7...PA0 */
#define VAL_GPIOACRH            0x888884b8      /* PA15...PA8 */
//#define VAL_GPIOACRH            0x888884B8      /* PA15...PA8 */
#define VAL_GPIOAODR            0xFFFFFFFF

/*
 * Port B setup.
 * Everything input with pull-up except:
 * PB0    - LED1
 * PB1    - LED2
 * PB7    - Push Pull output  (USB switch).
 */
#define VAL_GPIOBCRL            0x88888888      /*  PB7...PB0 */
#define VAL_GPIOBCRH            0x33838888      /* PB15...PB8 */
//#define VAL_GPIOBCRH            0x88888388      /* PB15...PB8 */
#define VAL_GPIOBODR            0xFFFFFFFF

/*
 * Port C setup.
 * Everything input with pull-up except:
 */
#define VAL_GPIOCCRL            0x88838888      /*  PC7...PC0 */
#define VAL_GPIOCCRH            0x88888888      /* PC15...PC8 */
#define VAL_GPIOCODR            0xFFFFFFFF

/*
 * Port D setup.
 * Everything input with pull-up except:
 * PD0  - Normal input (XTAL).
 * PD1  - Normal input (XTAL).
 */
#define VAL_GPIODCRL            0x88888888      /*  PD7...PD0 */
#define VAL_GPIODCRH            0x88888888      /* PD15...PD8 */
#define VAL_GPIODODR            0xFFFFFFFF

/*
 * Port E setup.
 * Everything input with pull-up except:
 */
#define VAL_GPIOECRL            0x88888888      /*  PE7...PE0 */
#define VAL_GPIOECRH            0x88888888      /* PE15...PE8 */
#define VAL_GPIOEODR            0xFFFFFFFF

/*
 * USB bus activation macro, required by the USB driver.
 */
/* The point is that most of the generic STM32F103* boards
   have a 1.5k resistor connected on one end to the D+ line
   and on the other end to some pin. Or even a slightly more
   complicated "USB enable" circuit, controlled by a pin.
   That should go here.

   However on some boards (e.g. one that I have), there's no
   such hardware. In which case it's better to not do anything.
*/

#define usb_lld_connect_bus(usbp) palClearPad(GPIOA, GPIOA_USB_DISC)

//#define usb_lld_connect_bus(usbp) palSetPadMode(GPIOA, 12, PAL_MODE_INPUT);

/*
 * USB bus de-activation macro, required by the USB driver.
 */

#define usb_lld_disconnect_bus(usbp) palSetPad(GPIOA, GPIOA_USB_DISC)

//#define usb_lld_disconnect_bus(usbp) palSetPadMode(GPIOA, 12, PAL_MODE_OUTPUT_PUSHPULL); palClearPad(GPIOA, 12);

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
