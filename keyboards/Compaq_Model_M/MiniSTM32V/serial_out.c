/*
copyright 2012 Robert Offner <100prozentoffner@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h" //ROB
#include "serial_out.h"
#include "config.h"
#include QMK_KEYBOARD_H
#include "translation_serial.h"
//##################################################################
// Function Prototypes
//##################################################################
void init_serial(void);
void make_serial(uint8_t keycode);
void brake_serial(uint8_t keycode);
void send_serial(uint8_t keycode);

//##################################################################
static BaseSequentialStream *const chout = (BaseSequentialStream *)&DEBUGPORT;  //ROB
//uint8_t led_status = (1<<6);
extern uint8_t led_status;
extern uint8_t key_repeat_time_init;
extern uint8_t key_repeat_time;
extern uint8_t keycode_repeat;
uint8_t modifier = 0;
/*
 * UART driver configuration structure.
 */
static UARTConfig uart_cfg_1 = {
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  600,
  0,
  0,
  0
};

void init_serial(void){
  uartStart(&UARTD2, &uart_cfg_1);
}

void brake_serial(uint8_t keycode){
  switch (keycode){
    case KC_LSFT:
    case KC_RSFT:
    modifier &= ~(1<<SHIFT);
    uartStartSend(&UARTD2, 1, "7"); //shift Lock off
    break;
    case KC_RCTL:
    case KC_LCTL:
    modifier &= ~(1<<STRG);
    break;
    case KC_LALT:
    modifier &= ~(1<<ALT);
    break;
    case KC_RALT:
    modifier &= ~(1<<ALTGR);
    break;
    default:
    break;
  }
  chprintf(chout, "break: kc: %0d mod: %0d \r\n", keycode, modifier);
  keycode_repeat = 0;
}

void make_serial(uint8_t keycode){
  switch (keycode){
    case KC_LSFT:
    case KC_RSFT:
    modifier |= (1<<SHIFT);
    uartStartSend(&UARTD2, 1, "6"); //shift Lock on
    //chprintf(chout, "modifier on: %0d \r\n", modifier);
    break;
    case KC_CAPS:
    if (led_status & (1<<USB_LED_CAPS_LOCK)){
      led_status &= ~(1<<USB_LED_CAPS_LOCK);
      modifier &= ~(1<<SHIFT);
      uartStartSend(&UARTD2, 1, "7"); //shift Lock off
    } else {
      led_status |= (1<<USB_LED_CAPS_LOCK);
      modifier |= (1<<SHIFT);
      uartStartSend(&UARTD2, 1, "6"); //shift Lock on
    }
    chprintf(chout, "modifier cf: %0d \r\n", modifier);
    break;
    case KC_RCTL:
    case KC_LCTL:
    modifier |= (1<<STRG);
    chprintf(chout, "modifier on: %0d \r\n", modifier);
    break;    
    case KC_LALT:
    modifier |= (1<<ALT);
    chprintf(chout, "modifier on: %0d \r\n", modifier);
    break;
    case KC_RALT:
    modifier |= (1<<ALTGR);
    chprintf(chout, "modifier on: %0d \r\n", modifier);
    break;
    case KC_NLCK:
    if (led_status & (1<<USB_LED_NUM_LOCK)){
      led_status &= ~(1<<USB_LED_NUM_LOCK);
    } else {
      led_status |= (1<<USB_LED_NUM_LOCK);
    }
    break;
    default:
    keycode_repeat = keycode;
    key_repeat_time = key_repeat_time_init * 6;
    send_serial(keycode);
    break;
  }
}

void repeat_serial_keycode(void){
	send_serial(keycode_repeat);
}

void send_serial(uint8_t keycode){
  uint8_t temp;
  switch (modifier){
    case SHIFT:
    temp = serial[keycode];
    //temp = serial_shift[keycode];
    //uartStartSend(&UARTD2, 1, '6'); //shift Lock on
    break;
    default:
    temp = serial[keycode];
    break;
  }
  chprintf(chout, "kc: %0d serial: %0d\r\n", keycode, temp);
  uartStartSend(&UARTD2, 1, &temp);
}
