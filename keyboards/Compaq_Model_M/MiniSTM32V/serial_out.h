
#include "ch.h"
#include "hal.h"
#include QMK_KEYBOARD_H

#pragma once

#define SHIFT 0
#define ALT 1
#define ALTGR 2
#define STRG 3

void init_serial(void);
void make_serial(uint8_t keycode);
void brake_serial(uint8_t keycode);
void repeat_serial_keycode(void);
void send_serial(uint8_t keycode);
