/*
Copyright 2012,2013 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include QMK_KEYBOARD_H
#include "ch.h"
#include "hal.h"
#include "chprintf.h" //ROB
#include "ibm_xt.h"
//#include "serial_out.h"
#include "remote.h"
/* These are the keys above the keyboard named with 1-4 and Enter, left of the Power Button.
See picture in the root directory of the RetroKVM Tree.
*/

//extern output_state_t output_state;
typedef enum {
  NO_REQ = 0,
  USB_ON,
  USB_TRANS,
  IBMXT_ON,
  REMOTE_ON,
  SERIAL_ON,
} output_state_t;
output_state_t output_state = USB_ON, request_state = NO_REQ;

enum Retro_KVM_keycodes {
  APP_1 = SAFE_RANGE,
  APP_2,
  APP_3,
  APP_4,
  APP_ENTR
};

// Define layer names
#define _NORMAL 0
#define _FNONE 1
#define _FNTWO 2

//#define USB_LED_USBOFF 5 //additional to defines 0..4 in led.h
//#define USB_LED_USBREALOFF 6 //additional to defines 0..4 in led.h

uint8_t key_repeat_time_init = KEY_REPEAT_TIME_INIT;
uint8_t key_repeat_time = KEY_REPEAT_TIME_INIT;
uint8_t keycode_repeat = 0; //, ext_pwr = 0;
extern const USBConfig usbcfg;
//uint8_t led_status = ((1<<USB_LED_USBREALOFF) | (1<<USB_LED_USBOFF));
uint8_t led_status = 0;


void change_state(output_state_t state);


//static BaseSequentialStream *const chout = (BaseSequentialStream *)&DEBUGPORT;  //ROB

// Highly Modified by Xydane
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*  [_NORMAL] = LAYOUT_seventy_ansi(
  KC_ESC, KC_F1,  KC_F2,  KC_F3,  KC_F4,  KC_F5,  KC_F6,  KC_F7,  KC_F8,  KC_F9,  KC_F10, KC_F11,  KC_F12,KC_NUMLOCK,KC_INSERT,KC_PSCREEN,KC_DELETE,KC_PAUSE, \
  KC_QUOTE, KC_1,   KC_2,    KC_3,    KC_4,    KC_5,     KC_6,     KC_7,      KC_8,     KC_9,     KC_0,      KC_MINS,    KC_EQL,                 KC_BSPC,     \
  KC_TAB,        KC_Q,   KC_W,   KC_E,   KC_R,    KC_T,       KC_Y,      KC_U,      KC_I,     KC_O,     KC_P,      KC_LBRC,       KC_RBRC,        KC_ENT,     \
  KC_CAPS,           KC_A,    KC_S,    KC_D,   KC_F,    KC_G,      KC_H,      KC_J,      KC_K,      KC_L,     KC_SCLN,    KC_QUOT,   KC_NONUS_HASH,           \
  KC_LSHFT, KC_BSLASH,   KZ_Z,   KC_X,   KC_C,    KC_V,   KC_B,     KC_N,     KC_M,      KC_COMM,       KC_DOT,     KC_SLSH,           KC_RSHFT,              \
  KC_LCTL,  MO(1),   KC_LGUI,   KC_LALT,                       KC_SPC,               KC_RALT,     KC_RGUI,      KC_RCTL,       KC_PGUP,    KC_UP,  KC_PGDOWN, \
                                                                                                                               KC_LEFT,   KC_DOWN, KC_RIGHT,  \
  ),
*/
  // ##################################### German Keyboard is working #########################################################################################
  [_NORMAL] = LAYOUT_seventy_ansi(
                                                                        APP_1,    APP_2,    APP_3,    APP_4,    APP_ENTR, \
  KC_ESC,   KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,    KC_F7,    KC_F8,    KC_F9,    KC_F10,   KC_F11,   KC_F12,    TG(2)  ,       KC_INSERT, KC_PAUSE, KC_DELETE, \
  KC_GRAVE, KC_1,     KC_2,     KC_3,     KC_4,     KC_5,     KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINUS, KC_EQL,    KC_BSPC, \
  KC_TAB,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,     KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_LBRC,  KC_RBRC,   KC_ENT,  \
  KC_CAPS,  KC_A,     KC_S,     KC_D,     KC_F,     KC_G,     KC_H,     KC_J,     KC_K,     KC_L,     KC_SCLN,  KC_QUOT,  KC_NONUS_HASH,     \
  KC_LSHIFT,KC_NUBS,KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_RSHIFT,   \
  KC_LCTL,  MO(1),    KC_LGUI,  KC_LALT,  KC_SPC,   KC_RALT,  KC_RGUI,  KC_RCTL,  KC_PGUP,  KC_UP,    KC_PGDOWN, \
                                                                                  KC_LEFT,  KC_DOWN,  KC_RIGHT \
  ),
 [_FNONE] = LAYOUT_seventy_ansi(
                                                                        KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, \
  KC_TRNS,  KC_MNXT,  KC_TRNS,  KC_MUTE,  KC_TRNS,  KC_TRNS,  KC_BRID,  KC_BRIU,  KC_VOLD,  KC_VOLU,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_SCROLLLOCK, KC_PSCR,   KC_TRNS,  KC_TRNS, \
  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS, \
  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS, \
  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   \
  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   \
  KC_TRNS,  XXXXXXX,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_HOME,  KC_TRNS,  KC_END,   \
                                                                                  KC_TRNS,  KC_TRNS,  KC_TRNS
  ),
[_FNTWO] = LAYOUT_seventy_ansi(
                                                                        KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, \
  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  XXXXXXX,  XXXXXXX,   KC_TRNS,       KC_TRNS,   KC_TRNS,  KC_TRNS, \
  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_7,     KC_8,     KC_9,     KC_PSLS,  XXXXXXX,  XXXXXXX,   KC_TRNS, \
  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_4,     KC_5,     KC_6,     KC_PAST,  XXXXXXX,  XXXXXXX,   KC_PENT, \
  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_1,     KC_2,     KC_3,     KC_PMNS,  XXXXXXX,  XXXXXXX,  \
  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_0,     KC_COMM,  KC_DOT,   KC_PPLS,  XXXXXXX,  \
  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS,   \
                                                                                  KC_TRNS,  KC_TRNS,  KC_TRNS \
  )
};

//__attribute__ ((weak))
//bool process_action_user(keyrecord_t *record) {
//    return true;
//}


/* Layer based ilumination, just binary */
layer_state_t layer_state_set_user(layer_state_t state) {
  switch (get_highest_layer(state)) {
  case _FNONE:
  print("\nLayer Set to 2\n");
//    palSetPad(GPIOA, 0);  //OFF Color A
//    palClearPad(GPIOA, 1); //ON Color B
    break;
  case _FNTWO:
  //palSetLine(LED2);
  print("\nLayer Set to 3\n");
//    palClearPad(GPIOA, 0); //ON Color A
//    palClearPad(GPIOA, 1);  //ON Color B
    break;
  default: //  for any other layers, or the default layer
  //palClearLine(LED2);
  print("\nLayer Set to 1\n");
//    palClearPad(GPIOA, 0); //ON Color A
//    palSetPad(GPIOA, 1);  //OFF Color B
    break;
  }
  return state;
}
void repeat_key(void){
  if (--key_repeat_time == 0 && keycode_repeat){
    key_repeat_time = key_repeat_time_init;
    //palToggleLine(DEBUGPIN);
    //dprintf("kcrep: %0x\n",keycode_repeat);
    switch (output_state){
      case USB_ON:
      break;
      case IBMXT_ON:
      repeat_XTkeycode();
      break;
      case REMOTE_ON:
      //repeat_NECkeycode();
      break;
      case SERIAL_ON:
      //repeat_serial_keycode(keycode);
      break;
      default:
      break;
    }
  }
}
//bool process_action_rob(keyrecord_t *record) {
//  chprintf(chout, "in process_record_quantum s\r\n");
//  return true;
//}
void post_process_record_kb(uint16_t keycode, keyrecord_t *record){
  //chprintf(chout, "in post_process_record_kb s\r\n");

  return;
}

void power_supply_toggle(void){
  chprintf(DBG,"Toggle\n");
  if (palReadLine(EXTPWR) == PAL_HIGH){
    chprintf(DBG,"ext. Power on.\n");
    palClearLine(EXTPWR);
  }
    else {
    palSetLine(EXTPWR);
    chprintf(DBG,"ext. Power off.\n");
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  //uint8_t kc_needs_processing = 0;
  // If console is enabled, it will print the matrix position and status of each key pressed
    //uprintf("KL: kc: %u, col: %u, row: %u, pressed: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed);
  //chprintf(DBG, "in process: %d \r\n", record->event.pressed);
#ifdef CONSOLE_ENABLE
    //dprintf("kc: %0x\n",keycode);
#endif 
  if (record->event.pressed){
//    if (led_status & (1<<USB_LED_USBOFF)){  //switch USB off AFTER keys have been released
//      led_status |= (1<<USB_LED_USBREALOFF);
//    }
//############################### Pins for External Power ON ################################
    if (keycode == APP_1){
      power_supply_toggle();
      return false;
    }
    chprintf(DBG,"Keycode: %02X\n", keycode);
    switch (output_state){
      case USB_ON:
        //kc_needs_processing = 1;
        break;
      case IBMXT_ON:
        make_XT(keycode);
        //kc_needs_processing = 0;
        break;
      case REMOTE_ON:
        make_remote(keycode);
        //kc_needs_processing = 0;
        break;
      case SERIAL_ON:
        //make_serial(keycode);
        //return false;
        break;
      default:
        break;
    }
  } else {
    switch (output_state){
      case USB_ON:
        //kc_needs_processing = 1;
        break;
      case IBMXT_ON:
        break_XT(keycode);
        //kc_needs_processing = 0;
        break;
      case REMOTE_ON:
        break_remote(keycode);
        //kc_needs_processing = 0;
        break;
      case SERIAL_ON:
        //break_serial(keycode);
        //return false;
        break;
      default:
        break;
    }
    if (request_state) change_state(request_state); // Chnge state after Button release
    
  }
  //led_set_user_2(led_status);
//  if (kc_needs_processing){
//    kc_needs_processing = 0;
//    return true;
//  }
//  else {
//    return false;
//  }
  return true;
}
void change_state(output_state_t state){
  output_state = state;
  request_state = 0;
  switch (state){
    case USB_ON:
    palSetLine(LED_USB);
    usbStart(&USBD1, &usbcfg);
    usbConnectBus(&USB_DRIVER);
    chprintf(DBG,"USB on\n");
    break;
    case IBMXT_ON:
    init_XT();
    chprintf(DBG,"IBM XT on\n");
    break;
    case REMOTE_ON:
    init_remote();
    chprintf(DBG,"REMOTE on\n");
    break;
    case SERIAL_ON:
    //init_serial();
    break;
    default:
    break;
  }
  if (output_state != USB_ON){
    palClearLine(LED_USB);
    usbStop(&USBD1);
    usbDisconnectBus(&USB_DRIVER);
  }
}
bool command_extra(uint8_t code){
    switch (code) {
      case KC_F1:
        request_state = USB_ON;
        break;
      case KC_F2:
        request_state = REMOTE_ON;
        break;        
      case KC_F3:
        request_state = IBMXT_ON;
        break;
      case KC_DEL:  //works
          chprintf(DBG,"Reset\n");
          break;
      case KC_HOME: //doesn't work
      case KC_END:  //doesn't work
      chprintf(DBG,"Click On\n");
      break;
      case KC_PGUP:
          chprintf(DBG,"LED all on\n");  //works
          break;
      case KC_PGDOWN:
          chprintf(DBG,"LED all off\n");  //works
          break;
      case KC_INSERT:                     //works
//          if (led_status & (1<<USB_LED_USBOFF)){
//            led_status &= ~(1<<USB_LED_USBOFF);
//            led_status &= ~(1<<USB_LED_USBREALOFF);
//            chprintf(DBG,"USB on\n");
//          } else {
//            led_status |= (1<<USB_LED_USBOFF);
//            chprintf(DBG,"USB off\n");
//          }
          break;
      default:
            chprintf(DBG,"Unknown extra command: %02X\n", code);
            return false;
    }
  return true;
}
//bool command_console_extra(uint8_t code) {
//  
//}

bool host_keyboard_send_user(void){ 
  if (output_state == USB_ON){
    return true;
  } else {
    return false;
  }  
}
//bool host_keyboard_send_user(void){ 
//  if (led_status & (1<<USB_LED_USBREALOFF)){
//    return false;
//  } else {
//    return true;
//  }  
//}

