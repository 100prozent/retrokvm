#BOOTMAGIC_ENABLE = yes	# Virtual DIP switch configuration
EXTRAKEY_ENABLE = no	# Audio control and System control
CONSOLE_ENABLE = no	# Console for debug
COMMAND_ENABLE = yes    # Commands for debug and configuration
SLEEP_LED_ENABLE = no  # Breathing sleep LED during USB suspend
NKRO_ENABLE = no	    # USB Nkey Rollover
CUSTOM_MATRIX = yes 	# Custom matrix file
MOUSEKEY_ENABLE = no

DEFAULT_FOLDER = RetroKVM/MiniSTM32V

