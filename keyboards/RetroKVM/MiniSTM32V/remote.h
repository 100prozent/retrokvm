
#include "ch.h"
#include "hal.h"
#include QMK_KEYBOARD_H

#pragma once

#define INVERT_NECSIGNAL 1
//#define NEC_DAT // see in board.h
void init_remote(void);
void make_remote(uint8_t keycode);
void break_remote(uint8_t keycode);
void repeat_NECkeycode(void);
void pwmpcb(PWMDriver *pwmp);
void pwmc1cb(PWMDriver *pwmp);