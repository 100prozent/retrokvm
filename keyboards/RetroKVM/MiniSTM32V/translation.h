#pragma once
const uint8_t PROGMEM IBM_XT[] = {

0x00, // 000	-----  
0x00, // 001	-----  
0x00, // 002	-----  
0x00, // 003	-----  
0x1e, // 004	A   
0x30, // 005	B   
0x2e, // 006	C   
0x20, // 007	D   
0x12, // 008	E   
0x21, // 009	F   
0x22, // 010	G   
0x23, // 011	H   
0x17, // 012	I   
0x24, // 013	J   
0x25, // 014	K   
0x26, // 015	L   
0x32, // 016	M   
0x31, // 017	N   
0x18, // 018	O   
0x19, // 019	P   
0x10, // 020	Q   
0x13, // 021	R   
0x1f, // 022	S   
0x14, // 023	T   
0x16, // 024	U   
0x2f, // 025	V   
0x11, // 026	W   
0x2d, // 027	X   
0x15, // 028	Y   (Z)
0x2c, // 029	Z   (Y)
0x02, // 030	1
0x03, // 031	2   
0x04, // 032	3   
0x05, // 033	4   
0x06, // 034	5   
0x07, // 035	6   
0x08, // 036	7   
0x09, // 037	8   
0x0a, // 038	9   
0x0b, // 039	0   
0x1c, // 040	Return   
0x01, // 041	Esc   
0x0e, // 042	Backspace   
0x0f, // 043	Tab   
0x39, // 044	Space   
0x0c, // 045	- _   (ß ?)
0x0d, // 046	+ =   (´ `)
0x1a, // 047	[     (Ü)
0x1b, // 048	]     (+ *)
0x2b, // 049	# '
0x00, // 050	-----  
0x27, // 051	; :   (Ö)
0x28, // 052	' “   (Ä)
0x29, // 053	Backquote  (^ °)
0x33, // 054	Comma   
0x34, // 055	Dot   
0x35, // 056	/ ?   
0x3a, // 057	Caps Lock   
0x3b, // 058	F1   
0x3c, // 059	F2   
0x3d, // 060	F3   
0x3e, // 061	F4   
0x3f, // 062	F5   
0x40, // 063	F6   
0x41, // 064	F7 
0x42, // 065	F8   
0x43, // 066	F9   
0x44, // 067	F10
0x57, // 068	F11  
0x58, // 069	F12  
0x37, // 070	PrtSc 
0x46, // 071	Scroll Lock   
0x61, // 072	Pause
0x52, // 073	Insert
0x47, // 074	Rule / Home   
0x49, // 075	PG UP
0x53, // 076	Delete
0x4f, // 077	END
0x51, // 078	PG DOWN
0x4d, // 079	Cursor RIGHT
0x4b, // 080	Cursor LEFT
0x50, // 081	Cursor DOWN
0x48, // 082	Cursor UP
0x45, // 083	Num Lock
0x00, // 084	-----  
0xe0, // 085	Print Screen
0x4a, // 086	Keypad minus   
0x4b, // 087	Keypad plus   
0x00, // 088	Keypad Enter 
0x4f, // 089	Keypad 1 / END
0x50, // 090	Keypad 2 / Cursor DOWN
0x51, // 091	Cursor 3 / PG DOWN
0x4b, // 092	Keypad 4 / Cursor LEFT
0x4c, // 093	Keypad 5 / -
0x4d, // 094	Keypad 6 / Cursor RIGHT
0x47, // 095	Keypad 7 / HOME
0x48, // 096	Keypad 8 / Cursor UP
0x49, // 097	Keypad 9 / PG UP
0x52, // 098	Keypad 0 / INS
0x53, // 099	Keypad , DEL
0x56, // 100	\ |   (< >)
0x6c, // 101	
0x00, // 102	  
0x00, // 103	  
0x00, // 104	  
0x00, // 105	  
0x00, // 106	  
0x00, // 107	  
0x00, // 108	  
0x00, // 109	  
0x00, // 110	  
0x00, // 111	  
0x00, // 112	  
0x00, // 113	  
0x00, // 114	  
0x00, // 115	  
0x00, // 116	  
0x00, // 117	  
0x00, // 118	  
0x00, // 119	  
0x00, // 120	  
0x00, // 121	  
0x00, // 122	  
0x00, // 123	  
0x00, // 124	  
0x00, // 125	  
0x00, // 126	  
0x00, // 127	  
0x00, // 128	  
0x00, // 129	  
0x00, // 130	  
0x00, // 131	  
0x00, // 132	  
0x00, // 133	  
0x00, // 134	  
0x00, // 135	  
0x00, // 136	  
0x00, // 137	  
0x00, // 138	  
0x00, // 139	  
0x00, // 140	  
0x00, // 141	  
0x00, // 142	  
0x00, // 143	  
0x00, // 144	  
0x00, // 145	  
0x00, // 146	  
0x00, // 147	  
0x00, // 148	  
0x00, // 149	  
0x00, // 150	  
0x00, // 151	  
0x00, // 152	  
0x00, // 153	  
0x00, // 154	  
0x00, // 155	  
0x00, // 156	  
0x00, // 157	  
0x00, // 158	  
0x00, // 159	  
0x00, // 160	  
0x00, // 161	  
0x00, // 162	  
0x00, // 163	  
0x00, // 164	  
0x00, // 165	  
0x00, // 166	  
0x00, // 167	  
0x00, // 168	  
0x00, // 169	  
0x00, // 170	  
0x00, // 171	  
0x00, // 172	  
0x00, // 173	  
0x00, // 174	  
0x00, // 175	  
0x00, // 176	  
0x00, // 177	  
0x00, // 178	  
0x00, // 179	  
0x00, // 180	  
0x00, // 181	  
0x00, // 182	  
0x00, // 183	  
0x00, // 184	  
0x00, // 185	  
0x00, // 186	  
0x00, // 187	  
0x00, // 188	  
0x00, // 189	  
0x00, // 190	  
0x00, // 191	  
0x00, // 192	  
0x00, // 193	  
0x00, // 194	  
0x00, // 195	  
0x00, // 196	  
0x00, // 197	  
0x00, // 198	  
0x00, // 199	  
0x00, // 200	  
0x00, // 201	  
0x00, // 202	  
0x00, // 203	  
0x00, // 204	  
0x00, // 205	  
0x00, // 206	  
0x00, // 207	  
0x00, // 208	  
0x00, // 209	  
0x00, // 210	  
0x00, // 211	  
0x00, // 212	  
0x00, // 213	  
0x00, // 214	  
0x00, // 215	  
0x00, // 216	  
0x00, // 217	  
0x00, // 218	  
0x00, // 219	  
0x00, // 220	  
0x00, // 221	  
0x00, // 222	  
0x00, // 223	  
0x1d, // 224	Left CTRL   
0x2a, // 225	Left SHIFT
0x38, // 226	Left ALT
0x00, // 227	-----
0x6e, // 228	ENTER
0x36, // 229	Right SHIFT
0x38, // 230	Right ALT
0x00, // 231	-----
0x00, // 232	-----
0x00, // 233	-----
0x00, // 234	-----
0x00, // 235	-----
0x00, // 236	-----
0x00, // 237	-----
0x00, // 238	-----
0x00, // 239	-----
0x00, // 240	-----
0x00, // 241	-----
0x00, // 242	-----
0x00, // 243	-----
0x00, // 244	-----
0x00, // 245	-----
0x00, // 246	-----
0x00, // 247	-----
0x00, // 248	-----
0x00, // 249	-----
0x00, // 250	-----
0x00, // 251	-----
0x00, // 252	-----
0x00, // 253	-----
0x00, // 254	-----
0x00  // 255	-----

};

