/*
copyright 2012 Robert Offner <100prozentoffner@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h" //ROB
#include "remote.h"
#include "config.h"
#include QMK_KEYBOARD_H

//##################################################################
// Function Prototypes
//##################################################################
void init_remote(void);
void NEC_send(uint8_t code);
void NEC_DAT_PASS(void);
//void repeat_NECkeycode(void);
//##################################################################
// Variables
//##################################################################
volatile uint8_t NEC_Repeat = 0;
volatile uint8_t count;
uint32_t data;

typedef enum {
	START_PWM,
	DATA,
	REPEAT1,
	REPEAT2,
	STOP_PWM
} PWM_state_t;

PWM_state_t PWM_state;

#define PWM_REVERSE

static PWMConfig pwmcfg = {
  200000,                                    /* 200kHz PWM clock frequency.   */
  2700,                                    // Initial PWM period 100mS.
  pwmpcb,
  {
#ifdef PWM_REVERSE
   {PWM_OUTPUT_ACTIVE_HIGH, pwmc1cb},
#else
   {PWM_OUTPUT_ACTIVE_LOW, pwmc1cb},
#endif
   {PWM_OUTPUT_DISABLED, NULL},
   {PWM_OUTPUT_DISABLED, NULL},
   {PWM_OUTPUT_DISABLED, NULL}
  },
  0,
  0,
#if STM32_PWM_USE_ADVANCED
  0
#endif
};

void init_PWM(void){
  pwmStart(&PWMD5, &pwmcfg);
  pwmEnablePeriodicNotification(&PWMD5);
  palSetPadMode(GPIOA, 0, PAL_MODE_STM32_ALTERNATE_PUSHPULL); //TIM5-CH1
  pwmEnableChannel(&PWMD5, 0, 1800);
  //pwmEnableChannel(&PWMD5, 0, PWM_PERCENTAGE_TO_WIDTH(&PWMD5, 5000));
  pwmEnableChannelNotification(&PWMD5, 0);
  //TIM5->CR1 &= ~TIM_CR1_CEN;
  pwmDisableChannel(&PWMD5, 0);
  count = 0;
  PWM_state = STOP_PWM;
}

void init_remote(void){
  palSetLineMode(SCART_CON1, PAL_MODE_OUTPUT_PUSHPULL);
  palSetLineMode(SCART_CON2, PAL_MODE_OUTPUT_PUSHPULL);
  palSetLineMode(SCART_CON3, PAL_MODE_OUTPUT_PUSHPULL);
  palSetLineMode(SCART_RGB_FBAS, PAL_MODE_OUTPUT_PUSHPULL);
  palSetLineMode(HDMI_SWITCH, PAL_MODE_OUTPUT_PUSHPULL);
  //NEC_DAT_PASS();
  init_PWM();
}
void SCART_Conv_send(uint8_t keycode){
  switch (keycode){
		case 1:
			palClearLine(SCART_CON1);
		break;
		case 2:
			palClearLine(SCART_CON2);
		break;
		case 3:
			palClearLine(SCART_CON3);
		break;
		case 4:
			palClearLine(HDMI_SWITCH);
		break;
  }
}
void scart_rgb_toggle(void){
  if (palReadLine(SCART_RGB_FBAS) == PAL_HIGH){
    chprintf(DBG,"SCART to RGB\n");
    palClearLine(SCART_RGB_FBAS);
  }
    else {
    palSetLine(SCART_RGB_FBAS);
    chprintf(DBG,"SCART to FBAS\n");
  }  
}

void make_remote(uint8_t keycode){
  switch (keycode){
  	// ##############################  Remote for TV-Board ########################
  	case KC_VOLU:  //A9
  	NEC_send(0x48);
  	break;
  	case KC_VOLD:  //AA
  	NEC_send(0x49);
  	break;
  	case KC_BRIU:  //BD
  	break;
  	case KC_BRID:  //BE
  	break;
  	case KC_MUTE:  //A8
  	NEC_send(0x01);
  	break;
  	case KC_UP:    //52
  	NEC_send(0x13);
  	break;
  	case KC_DOWN:  //51
  	NEC_send(0x14);
  	break;
  	case KC_LEFT:  //50
  	NEC_send(0x11);
  	break;
  	case KC_RIGHT: //4F
  	NEC_send(0x12);
  	break;
  	case KC_ENT:   //28
   	NEC_send(0x10);
 		break;
  	case KC_1:     //1E
  	NEC_send(0x02);
  	break;
  	case KC_2:     //1F
  	NEC_send(0x03);
  	break;
  	case KC_3:     //20
  	NEC_send(0x04);
  	break;
  	case KC_4:     //21
  	NEC_send(0x05);
  	break;
  	case KC_5:     //22
  	NEC_send(0x06);
  	break;
  	case KC_6:     //23
  	NEC_send(0x07);
  	break;
  	case KC_7:     //24
  	NEC_send(0x08);
  	break;
  	case KC_8:     //25
  	NEC_send(0x09);
  	break;
  	case KC_9:     //26
  	NEC_send(0x0A);
  	break;
  	case KC_0:     //27
  	NEC_send(0x0C);
  	break;
  	case KC_ESC:   //29
  	NEC_send(0x15);
  	break;
  	case KC_F10:   //43
  	NEC_send(0x0f);
  	break;
  	case KC_F5:    //3E
  	NEC_send(0x16);
  	break;
  	case KC_SLSH:  //38 (is '-'' on german Layout)
  	NEC_send(0x0b);
  	break;
  	// #################  Remote for SCART -> HDMI Converter ####################
  	case KC_S:    //16
  	SCART_Conv_send(0x1);
  	break;
  	case KC_R:    //15
  	SCART_Conv_send(0x2);
  	break;
  	case KC_P:    //13
  	SCART_Conv_send(0x3);
  	break;
  	case KC_H:    //
  	SCART_Conv_send(0x4);
  	break;
  	case KC_F:    //09
  	scart_rgb_toggle();
  	break;
  }
}
void break_remote(uint8_t keycode){
	NEC_Repeat = 0;
	//keycode_repeat = 0;
	chprintf(DBG,"break\n");
	palSetLine(SCART_CON1);
	palSetLine(SCART_CON2);
	palSetLine(SCART_CON3);
	palSetLine(HDMI_SWITCH);
}
//void NEC_DAT_ACT(void){
//#if INVERT_NECSIGNAL == 1
//  palSetLine(NEC_DAT);
//#else
//  palClearLine(NEC_DAT);
//#endif  
//}
//void NEC_DAT_PASS(void){
//#if INVERT_NECSIGNAL == 1
//  palClearLine(NEC_DAT);
//#else
//  palSetLine(NEC_DAT);
//#endif 
//}
//void NEC_START(void){
//	NEC_DAT_ACT();
//	osalThreadSleepMilliseconds(9);
//	//_delay_micro(250);
//	NEC_DAT_PASS();
//	osalThreadSleepMilliseconds(4);
//	_delay_micro(500);
//	//chprintf(DBG,"carrier sent\n");
//}
//void send_NULL(void){
//	NEC_DAT_ACT();
//	_delay_micro(565);
//	NEC_DAT_PASS();
//	_delay_micro(565);
//}
//void send_ONE(void){
//	NEC_DAT_ACT();
//	_delay_micro(565);
//	NEC_DAT_PASS();
//	_delay_micro(1690);
//}
//void NEC_STOP(void){
//	NEC_DAT_ACT();
//	_delay_micro(565);
//	NEC_DAT_PASS();
//}
//void NEC_Rep(void){
//	osalThreadSleepMilliseconds(41);
//	NEC_DAT_ACT();
//	osalThreadSleepMilliseconds(9);
//	NEC_DAT_PASS();
//	_delay_micro(2250);
//	NEC_DAT_ACT();
//	_delay_micro(565);
//	NEC_DAT_PASS();
//}
//void NEC_Byte(uint8_t value){
//	uint8_t i;
	//chprintf(DBG,"send: %x\n", value);
//	for (i=0; i < 8; i++){
//		(value & 1) ? send_ONE() : send_NULL();
//		value = value >> 1;
//	}
//}
//void repeat_NECkeycode(void){
//	NEC_send(keycode_repeat);
//}



void NEC_ONE_PWM(void){
	pwmChangePeriod(&PWMD5, 450);     // 560us + 1690us
	pwmEnableChannel(&PWMD5, 0, 112); // 560us
}
void NEC_NULL_PWM(void){
	pwmChangePeriod(&PWMD5, 224);     // 560us + 560us
	pwmEnableChannel(&PWMD5, 0, 112); // 560us
}
void NEC_STOPBIT_PWM(void){
	PWM_state = REPEAT1;
	//PWM_state = STOP_PWM;
	pwmChangePeriod(&PWMD5, 8312);    // 560us + 41ms
	pwmEnableChannel(&PWMD5, 0, 112); // 560us
}
void NEC_REPEAT_PWM1(void){
	PWM_state = REPEAT2;
	pwmChangePeriod(&PWMD5, 2254);     // 9ms + 2.27ms
	pwmEnableChannel(&PWMD5, 0, 1800); // 9ms
}
void NEC_REPEAT_PWM2(void){
	if (NEC_Repeat){
		PWM_state = REPEAT1;
	}
	else{
		PWM_state = STOP_PWM;
	}
	pwmChangePeriod(&PWMD5, 19212);   // 560us + 95.5ms
	pwmEnableChannel(&PWMD5, 0, 112); // 560us
}
void NEC_DATA_PWM(void){
	PWM_state = DATA;
	count = 32 ;
	(data & 1) ? NEC_ONE_PWM() : NEC_NULL_PWM();
	data >>= 1;
}
void pwmpcb(PWMDriver *pwmp) {
  (void)pwmp;
  switch (PWM_state){
  	case START_PWM:
  	  NEC_DATA_PWM();
  	break;
  	case DATA:
		if (--count == 0){
			NEC_STOPBIT_PWM();
		}
		else {
			(data & 1) ? NEC_ONE_PWM() : NEC_NULL_PWM();
			data >>= 1;
		}
  	break;
    case REPEAT1:
        NEC_REPEAT_PWM1();
    break;
    case REPEAT2:
        NEC_REPEAT_PWM2();
    break;  	
    case STOP_PWM:
        pwmDisableChannel(&PWMD5, 0);
    break;
  }

}
void pwmc1cb(PWMDriver *pwmp) {

  (void)pwmp;
}
void NEC_START_PWM(void){
	PWM_state = START_PWM;
	pwmChangePeriod(&PWMD5, 2700); // Writes a new value (pw-1) in TIMx_ARR Register
	pwmEnableChannel(&PWMD5, 0, 1800);
}
void NEC_send(uint8_t code){
	chprintf(DBG,"make\n");
	NEC_Repeat = 1;
	data = 0x0000bf00;  // data is sent LSB first, LSB is Address (0xBF), LLSB = 0
	data |= (uint32_t)(code << 16);  // MSB = code
	data |= (uint32_t)(~code << 24); // MMSB = ~code
	//chprintf(DBG,"data: %x\n", data);
    NEC_START_PWM();
//    NEC_Byte(0);     //Address Hi
//    NEC_Byte(0xBF);  //Address Lo
//    NEC_Byte(code);  //Command Hi 
//    NEC_Byte(~code); //Command Lo 
//    NEC_STOP();
}
